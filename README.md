# Radiant 

Raytracer for a personal learning experience.

## "Done"

- BVH implementation
- Basic ray-casting
- Triangle intersection
- OpenMP (CPU) acceleration

## TODO

Pretty much everything interesting and what is to be "explored" here. :)

- Manual camera setup
  * Reading PBRT files would be nice here for a good bunch of nice test scenes.
    Also might want to have Assimp handle (parts) of this?
- GUI app to move around the scene (likely render with OpenGL) and raytrace
  on user request
- Accelerate the tracing computation with OpenCL/CUDA
- Re-work the structure holding scene information from AoS to SoA
  * Might potentially make much sense for re-locating computation on GPU, too.
- Actually apply some rendering tricks that benefit from a raytracing approach
  * spectrum stuff
  * path tracing
  * whatnot?

Others, more of the nice-to-have variety.

- Fast (GPU) BVH construction for fun and profit?

#ifndef MODELIO_HPP
#define MODELIO_HPP

#include "content/aiscene.hpp"
#include "model.hpp"

#include <iosfwd>


Model buildModel(const aiScene &scene, std::ostream *out);

#endif // MODELIO_HPP

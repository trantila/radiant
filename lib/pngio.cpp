#include "pngio.hpp"

#include <png.h>

#include <iostream>
#include <stdexcept>
#include <memory>
#include <limits>
#include <cstdio>


namespace {

// NOTE PNG_TRANSFORM_SWAP_ENDIAN or the like is needed if using 16bpc depth
using PixelChannel = uint8_t;
constexpr unsigned bitdepth = 8u;
constexpr unsigned maxvalue = std::numeric_limits<PixelChannel>::max();
constexpr unsigned nchannels = 3u;

using namespace Eigen;

using PngBuffer = Matrix<PixelChannel, nchannels, Dynamic>;
using PngPixel = Matrix<PixelChannel, nchannels, 1>;

}


void writeAsPng(const Image &img, FILE *file) {
    const auto width = img.width();
    const auto height = img.height();
    PngBuffer buffer = PngBuffer::Zero(3, width * height);

    const Array3f rgbMax(maxvalue);

    auto rows = std::unique_ptr<png_bytep[]>(new png_bytep[height]);
    for (unsigned row = 0; row < height; ++row) {
        // Pick address of the first element of first pixel on each row
        rows[row] = reinterpret_cast<png_bytep>(&buffer(0, row * width));
        for (unsigned col = 0; col < width; ++col) {
            auto pixelRgb = img.pixel(row, col).head<3>();
            auto rgb = pixelRgb.array() * rgbMax;
            PngPixel pixel = rgb.cast<PixelChannel>();
            buffer.col(row * width + col) = pixel;
        }
    }

    png_structp pngWriter = png_create_write_struct(PNG_LIBPNG_VER_STRING, &std::cerr, [](png_structp, png_const_charp msg) {
        throw std::logic_error(msg ? msg : "Creating a PNG writer failed critically");
    }, [](png_structp pngWriter, png_const_charp msg) {
        std::ostream &out = *static_cast<std::ostream *>(png_get_error_ptr(pngWriter));
        out << (msg ? msg : "Something fishy about creating PNG write struct!") << std::endl;
    });
    if (!pngWriter) throw std::logic_error("Creating a PNG writer failed");

    png_infop png = png_create_info_struct(pngWriter);
    if (!png) {
        png_destroy_write_struct(&pngWriter, &png);
        throw std::logic_error("Creating a PNG presentation failed!");
    }

    png_init_io(pngWriter, file ? file : stdout);
    //    png_set_sig_bytes(pngWriter, 8); // TODO ???
    png_set_IHDR(pngWriter, png, width, height, bitdepth,
                 PNG_COLOR_TYPE_RGB, PNG_INTERLACE_NONE, PNG_COMPRESSION_TYPE_DEFAULT, PNG_TRANSFORM_IDENTITY);

    png_set_rows(pngWriter, png, rows.get());

    png_write_png(pngWriter, png, PNG_TRANSFORM_IDENTITY, nullptr);
}

void saveAsPng(const Image &img, const std::string &filename) {
    // NOTE std::optional requires some footwork on referenciness since
    //      references can't be stored. C++11 introduces std::reference_wrapper
    //      for this purpose but this would make the API just excessive.
    //      Better KISS and go with string and FILE* (both of which anyway have
    //      a natural-ish "empty" value anyway).
    unique_file_ptr file = filename.empty() ? nullptr : open_file(filename.c_str(), "wb");

    writeAsPng(img, file.get());
}

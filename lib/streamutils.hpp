#ifndef STREAMUTILS_HPP
#define STREAMUTILS_HPP

#include <istream>
#include <ostream>


template<typename THead>
std::ostream& stream(std::ostream& out, THead content) {
    return out << content;
}

template<typename THead, typename... TTails>
std::ostream& stream(std::ostream& out, THead contentHead, TTails... contentTail) {
    out << contentHead;
    return stream(out, contentTail...);
}

#endif // STREAMUTILS_HPP

#ifndef PNGIO_HPP
#define PNGIO_HPP

#include "cfile.hpp"
#include "image.hpp"

#include <string>


/**
 * @brief Write image to file as PNG.
 * @param img       The image to save.
 * @param file      FILE to write to, or NULL for stdout.
 */
void writeAsPng(const Image &img, FILE *file);

/**
 * @brief Save image as PNG.
 * @param img       The image to save.
 * @param filename  The destination filename or empty for stdout.
 */
void saveAsPng(const Image &img, const std::string &filename);

#endif // PNGIO_HPP

#ifndef SCENEIO_HPP
#define SCENEIO_HPP

#include "aiscene.hpp"

#include <stdexcept>
#include <ostream>
#include <string>
#include <memory>

namespace content
{
namespace impl
{
struct aiSceneConstDeleter final { void operator()(const aiScene *scene); };
}

using aiSceneConstUniquePtr = std::unique_ptr<const aiScene, impl::aiSceneConstDeleter>;

class aiSceneLoadingError : public std::runtime_error{
public:
    aiSceneLoadingError(const std::string &message);
};

/// Load an Assimp scene. Returns a valid pointer unless there was a fatal error in which case AssimpSceneLoadingError is thrown.
aiSceneConstUniquePtr loadAssimpScene(const std::string &filepath);

}

#endif // SCENEIO_HPP

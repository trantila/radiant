#include "aisceneio.hpp"

#include <assimp/cimport.h>
#include <assimp/postprocess.h>


namespace content
{

void impl::aiSceneConstDeleter::operator()(const aiScene *scene) {
    aiReleaseImport(scene);
}

aiSceneLoadingError::aiSceneLoadingError(const std::string &message)
    : std::runtime_error(message) {}

aiSceneConstUniquePtr loadAssimpScene(const std::string &filepath) {
    unsigned assimpFlags = 0x00
            // TODO Sharp corners much..?
            | aiProcess_JoinIdenticalVertices
            | aiProcess_Triangulate
            | aiProcess_ValidateDataStructure;

    auto scene = aiImportFile(filepath.c_str(), assimpFlags);

    if (!scene) {
        auto errorMessage = aiGetErrorString();
        throw aiSceneLoadingError(errorMessage);
    }

    if (scene->mFlags & AI_SCENE_FLAGS_INCOMPLETE) {
        throw aiSceneLoadingError("Read Assimp scene was \"incomplete\" as per Assimp itself.");
    }

    // NOTE There is also AI_SCENE_FLAGS_VALIDATION_WARNING which is set rather light-heartedly as per Assimp docs.
    //      The idea seems to be that the scene *should* be usable but *could* be somehow illogical or malformed,
    //      i.e. bone weights not summing up to one, referenced textures not found or the like.
    // TODO There should be some log where Assimp outputs this stuff in more detail, but where or how..?

    return aiSceneConstUniquePtr(scene);
}

}

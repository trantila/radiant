#ifndef AISCENE_H
#define AISCENE_H

#include <assimp/scene.h>
#include <assimp/material.h>
#include <assimp/mesh.h>
#include <assimp/types.h>

#include <nonstd/span.hpp>

#include <optional>


namespace content
{
    inline nonstd::span<aiMaterial*> getMaterials(const aiScene &scene) {
        return nonstd::span(scene.mMaterials, scene.mNumMaterials);
    }

    inline nonstd::span<aiMesh*> getMeshes(const aiScene &scene) {
        return nonstd::span(scene.mMeshes, scene.mNumMeshes);
    }

    inline nonstd::span<aiNode*> getChildren(const aiNode &node) {
        return nonstd::span(node.mChildren, node.mNumChildren);
    }

    inline nonstd::span<unsigned> getMeshIndices(const aiNode &node) {
        return nonstd::span(node.mMeshes, node.mNumMeshes);
    }

    inline nonstd::span<aiFace> getFaces(const aiMesh &mesh) {
        return nonstd::span(mesh.mFaces, mesh.mNumFaces);
    }

    inline nonstd::span<aiVector3D> getVertexPositions(const aiMesh &mesh) {
        return nonstd::span(mesh.mVertices, mesh.mNumVertices);
    }

    inline nonstd::span<aiColor4D* const, AI_MAX_NUMBER_OF_COLOR_SETS> getVertexColorSets(const aiMesh &mesh) {
        return nonstd::span(mesh.mColors);
    }

    inline std::optional<nonstd::span<aiColor4D>> getVertexColors(const aiMesh &mesh, unsigned colorSet) {
        if (!mesh.HasVertexColors(colorSet))
            return std::nullopt;

        auto colorSets = getVertexColorSets(mesh);
        return nonstd::span(colorSets[colorSet], mesh.mNumVertices);
    }
}


//
// Assimp "std" utilities; should reside in the same namespace as the relevant parts of Assimp itself, i.e. the global one.
//

std::ostream & operator<< (std::ostream &out, const aiString &string);

#endif // AISCENE_H

#include "aiscene.hpp"

#include <ostream>


std::ostream & operator<< (std::ostream &out, const aiString &string) {
    return out << string.C_Str();
}

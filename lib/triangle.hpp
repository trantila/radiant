#ifndef TRIANGLE_HPP
#define TRIANGLE_HPP

#include "material.hpp"
#include "aabb.hpp"
#include "math.hpp"


struct Triangle {
    Vector3f vertices[3];
    Vector3f colors[3];
    Material *material = nullptr;

    AABB3f aabb() const {
        return AABB3f(vertices[0]).extend(vertices[1]).extend(vertices[2]);
    }

    Vector3f centroid() const {
        return (vertices[0] + vertices[1] + vertices[2]) / 3.f;
    }

    Vector3f normal() const {
        auto e1 = vertices[1] - vertices[0];
        auto e2 = vertices[2] - vertices[0];
        return e1.cross(e2).normalized();
    }
};


#endif // TRIANGLE_HPP

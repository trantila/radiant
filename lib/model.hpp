#ifndef MODEL_HPP
#define MODEL_HPP

#include "triangle.hpp"
#include "material.hpp"

#include <vector>


class Model
{
    std::vector<Triangle> _triangles;
    std::vector<Material> _materials;

public:
    Model(std::vector<Triangle> &&triangles, std::vector<Material> &&materials)
        : _triangles(triangles), _materials(materials) {}

    const std::vector<Triangle> &triangles() const { return _triangles; }
    const std::vector<Material> &materials() const { return _materials; }
};

#endif // MODEL_HPP

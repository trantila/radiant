#include "triangleintersection.hpp"


TriangleIntersection mollerTrumboreIntersect(
        const Vector3f &rayOrigin,
        const Vector3f &rayVector,
        const Triangle &triangle,
        float epsilon)
{
    TriangleIntersection intersection;

    Vector3f v0 = triangle.vertices[0];
    Vector3f v1 = triangle.vertices[1];
    Vector3f v2 = triangle.vertices[2];
    Vector3f e1 = v1 - v0;
    Vector3f e2 = v2 - v0;
    Vector3f h = rayVector.cross(e2);
    float a = e1.dot(h);

    // Parallelity check?
    if (a > -epsilon && a < epsilon)
        return intersection;

    Vector3f s = rayOrigin - v0;
    float f = 1/a;
    intersection.u = f * (s.dot(h));

    if (intersection.u < 0.f || intersection.u > 1.f)
        return intersection;

    Vector3f q = s.cross(e1);
    intersection.v = f * rayVector.dot(q);

    if (intersection.v < 0.f || intersection.u + intersection.v > 1.f)
        return intersection;

    intersection.t = f * e2.dot(q);

    return intersection;
}


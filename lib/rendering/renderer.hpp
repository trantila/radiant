#ifndef RENDERER_HPP
#define RENDERER_HPP

#include "ishader.hpp"
#include "raycaster.hpp"
#include "image.hpp"
#include "math.hpp"


/// Handles shading matters and actually fills up an image
class Renderer
{
public:
    // TODO Proper Camera/RayGenerator class with all camera details, e.g. fov, perspective, ...
    //      In short a ray generator with given camera params.
    using Camera4f = Eigen::Matrix4f;

private:
    const RayCaster &rayCaster;
    float cutOff;

public:
    Renderer(const RayCaster &rayCaster, float cutOff)
        : rayCaster(rayCaster), cutOff(cutOff) { }

    void render(Image &image, const Camera4f &camera, float fov, const IShader &shader);
};

#endif // RENDERER_HPP

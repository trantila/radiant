#ifndef DEPTHSHADER_HPP
#define DEPTHSHADER_HPP

#include "ishader.hpp"
#include "raycaster.hpp"
#include "math.hpp"


class DepthShader : public IShader
{
    float capDistance;
public:
    DepthShader(float capDistance) : capDistance(capDistance) {}

    Vector3f shade(const RayCaster::Result &result, const Vector3f &cameraPos) const override;
};

#endif // DEPTHSHADER_HPP

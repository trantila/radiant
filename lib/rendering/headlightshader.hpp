#ifndef HEADLIGHTSHADER_HPP
#define HEADLIGHTSHADER_HPP

#include "ishader.hpp"


class HeadlightShader : public IShader
{
public:
    HeadlightShader() {}

    Vector3f shade(const RayCaster::Result &result, const Vector3f &cameraPos) const override;
};

#endif // HEADLIGHTSHADER_HPP

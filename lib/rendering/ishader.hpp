#ifndef ISHADER_HPP
#define ISHADER_HPP

#include "raycaster.hpp"
#include "math.hpp"


class IShader {
public:
    virtual Vector3f shade(const RayCaster::Result &result, const Vector3f &cameraPos) const = 0;

    virtual ~IShader() {}
};


#endif // ISHADER_HPP

#include "headlightshader.hpp"


Vector3f HeadlightShader::shade(const RayCaster::Result &result, const Vector3f &cameraPos) const {
    auto *material = result.triangle->material;

    Vector3f normal = result.triangle->normal();
    Vector3f diffuse = Vector3f::Zero();
    Vector3f specular = Vector3f::Zero();

    if (material) {
        diffuse = material->diffuse;
        specular = material->specular;
        // if (useTextures && existingTextures)
        //     getTextureParameters(hit, diffuse, normal, specular);
    } else {
        const auto &colors = result.triangle->colors;
        diffuse = (1.f - result.u - result.v) * colors[0]
                + result.u * colors[1]
                + result.v * colors[2];
    }

    Vector3f dirToCamera = (cameraPos - result.point).normalized();
    float alignment = std::abs(normal.dot(dirToCamera));

    return diffuse * alignment;
}

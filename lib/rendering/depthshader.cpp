#include "depthshader.hpp"


Vector3f DepthShader::shade(const RayCaster::Result &result, const Eigen::Vector3f &) const
{
    float intensity = 1.f - std::min(std::max(result.t, 0.f), capDistance) / capDistance;
    return Vector3f::Ones() * intensity;
}

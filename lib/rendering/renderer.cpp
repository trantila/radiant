#include "renderer.hpp"

void Renderer::render(Image &image, const Camera4f &camera, float fov, const IShader &shader) {
    const auto width = image.width();
    const auto height = image.height();

    const float invAspectRatio = ratio(height, width);
    const float horizontalHalfFov = fov / 2.f;
    const float verticalHalfFov = horizontalHalfFov * invAspectRatio;

    Vector3f cameraDirInCamera = Vector3f::UnitZ();
    Vector3f horizontalSpan = Vector3f::UnitX() * std::tan(horizontalHalfFov) * 2.f;
    Vector3f verticalSpan = Vector3f::UnitY() * std::tan(verticalHalfFov) * 2.f;

    Vector3f rayOrigin = camera.col(3).hnormalized();

    #pragma omp parallel for // OMP 2 requires signed iterand
    for (ptrdiff_t row = 0; row < height; ++row) {
        // k = [-0.5, 0.5)
        float ky = ratio(row, height) - 0.5f;

        for (ptrdiff_t col = 0; col < width; ++col) {
            float kx = ratio(col, width) - 0.5f;

            auto denormRayDirInCamera = cameraDirInCamera + kx * horizontalSpan + ky * verticalSpan;
            auto rayDirInCamera = denormRayDirInCamera.normalized();
            auto rayDir = camera.topLeftCorner<3, 3>() * rayDirInCamera;
            Vector3f rayVector = rayDir * cutOff;

            auto result = rayCaster.cast(rayOrigin, rayVector);
            Vector3f color;
            if (result.hit())
                color = shader.shade(result, rayOrigin);

            image.pixel(row, col) = Image::Pixel(color.x(), color.y(), color.z(), 0.f);
        }
    }
}

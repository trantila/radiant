#include "modelbuilding.hpp"

#include "content/aisceneio.hpp"
#include "streamutils.hpp"
#include "math.hpp"

#include <nonstd/span.hpp>

#include <ostream>
#include <stack>
#include <vector>
#include <string>
#include <tuple>
#include <algorithm>


using namespace content;

namespace
{
template <typename... TTypes>
void tryStreamLine(std::ostream *out, TTypes... values) {
    if (out) {
        stream(*out, values...) << std::endl;
    }
}

//
// "Dangerous" Assimp-Eigen utilities, that trust the ABIs to stay stable and match on the relevant details.
//

template <typename TScalar>
auto asEigenMap(aiVector3t<TScalar> &vector) {
    return Eigen::Map<Eigen::Matrix<TScalar, 3, 1>>(&vector.x);
};

template <typename TScalar>
auto asEigenMap(aiColor4t<TScalar> &vector) {
    return Eigen::Map<Eigen::Matrix<TScalar, 4, 1>>(&vector.r);
};

auto asEigenMap(aiColor3D &vector) {
    return Eigen::Map<Eigen::Vector3f>(&vector.r);
};


enum class ColorProperty {
    Diffuse = 0,
    Specular,
    Ambient,
    Emissive,
};

Vector3f getMaterialColor(const aiMaterial &material, ColorProperty colorProperty) {
    aiColor3D color;
    switch (colorProperty) {
    case ColorProperty::Diffuse:
        material.Get(AI_MATKEY_COLOR_DIFFUSE, color);
        break;
    case ColorProperty::Specular:
        material.Get(AI_MATKEY_COLOR_SPECULAR, color);
        break;
    case ColorProperty::Ambient:
        material.Get(AI_MATKEY_COLOR_AMBIENT, color);
        break;
    case ColorProperty::Emissive:
        material.Get(AI_MATKEY_COLOR_EMISSIVE, color);
        break;
    }
    return asEigenMap(color);
}

Triangle createTriangle(aiFace &face, aiMesh &mesh, Material *material)
{
    auto vertexPositions = getVertexPositions(mesh);
    auto maybeVertexColors = getVertexColors(mesh, 0);

    Triangle triangle;
    triangle.material = material;

    // This trickery with vertex data iteration spares points and lines as degenerate triangles.
    // TODO Set-up Assimp such that there is no degenerate triangles!
    Eigen::Vector3f vertexPosition = Eigen::Vector3f::Zero();
    Eigen::Vector3f vertexColor = Eigen::Vector3f::Ones();
    for (unsigned k = 0; k < 3; ++k) {
        if (k < face.mNumIndices) {
            unsigned index = face.mIndices[k];

            vertexPosition = asEigenMap(vertexPositions[index]);

            if (maybeVertexColors) {
                auto vertexColors = *maybeVertexColors;
                // NOTE Alpha channel is available and forced off.
                vertexColor = asEigenMap(vertexColors[index]).head<3>();
            }
        }
        triangle.vertices[k] = vertexPosition;
        triangle.colors[k] = vertexColor;
    }

    return triangle;
}

}


Model buildModel(const aiScene &scene, std::ostream *out) {
    auto meshes = getMeshes(scene);
    auto materials = getMaterials(scene);
    aiNode *rootNode = scene.mRootNode;

    tryStreamLine(out, "Scene:\t", rootNode->mName);
    tryStreamLine(out, "-- meshes #:\t", meshes.size());
    tryStreamLine(out, "-- materials #:\t", materials.size());
    tryStreamLine(out, "-- textures #:\t", scene.mNumTextures);
    tryStreamLine(out, "-- nodes []:");

    std::vector<Material> modelMaterials;
    std::transform(materials.begin(), materials.end(), std::back_inserter(modelMaterials), [](aiMaterial *material) {
        Vector3f diffuse = getMaterialColor(*material, ColorProperty::Diffuse);
        Vector3f specular = getMaterialColor(*material, ColorProperty::Specular);
        return Material { diffuse, specular };
    });

    std::vector<Triangle> modelTriangles;
    std::stack<std::tuple<aiNode*, aiMatrix4x4>> stack;
    stack.push(std::make_tuple(rootNode, aiMatrix4x4()));
    while (!stack.empty()) {
        aiNode* node;
        aiMatrix4x4 parentToWorld;
        std::tie(node, parentToWorld) = stack.top();
        stack.pop();

        tryStreamLine(out, "\tNode:\t", node->mName);
        tryStreamLine(out, "\t-- meshes #:\t", node->mNumMeshes);
        tryStreamLine(out, "\t-- children #:\t", node->mNumChildren);

        aiMatrix4x4 nodeToWorld = node->mTransformation * parentToWorld;

        for (unsigned meshIdx : getMeshIndices(*node)) {
            aiMesh &mesh = *meshes[meshIdx];
            unsigned materialIndex = mesh.mMaterialIndex;

            // NOTE This might be somewhat conservative as Assimp is apparently *always* creating at least one (dummy) material.
            Material *material = materialIndex < modelMaterials.size() ? &modelMaterials[materialIndex] : nullptr;

            for (aiFace &face : getFaces(mesh)) {
                Triangle triangle = createTriangle(face, mesh, material);
                modelTriangles.push_back(triangle);
            }
        }

        for (auto &childNode : getChildren(*node))
            stack.push(std::make_tuple(childNode, nodeToWorld));
    }

    return Model(std::move(modelTriangles), std::move(modelMaterials));
}

#include "bvhsahsplitter.hpp"

#include <algorithm>
#include <numeric> // accumulate
#include <functional>


#ifndef SAH_BINS_PER_DIM
#define SAH_BINS_PER_DIM 8
#endif
static_assert(SAH_BINS_PER_DIM > 0, "Must be >0 number of bins to test per dimension");


namespace {
using namespace acceleration;

std::function<bool(size_t, size_t)> dimensionwiseIndexedTriangleComparer(
    const std::vector<Triangle> &triangles,
    int splitDimension
) {
    return [&triangles, splitDimension](size_t l, size_t r) {
        auto lc = triangles[l].centroid();
        auto rc = triangles[r].centroid();
        return lc[splitDimension] < rc[splitDimension];
    };
}

std::function<bool(size_t)> dimensionwiseIndexedTrianglePartitioner(
    const std::vector<Triangle> &triangles,
    int splitDimension,
    float splitValue
) {
    return [&triangles, splitDimension, splitValue](size_t i) {
        const auto &triangle = triangles[i];
        return triangle.centroid()[splitDimension] < splitValue;
    };
}

}


namespace acceleration {

BvhSahSplitter::IndexIterator BvhSahSplitter::operator ()(const AABB &box, IndexIterator begin, IndexIterator end) {
    const ptrdiff_t n = end - begin;
    const ptrdiff_t nsplits = n - 1;

    ptrdiff_t bestPivot = 0;
    float bestCost = std::numeric_limits<float>::max();
    int bestDimension = -1;

    // Initialize holders for left and right costs
    auto lcosts = std::unique_ptr<float[]>(new float[nsplits]);
    auto rcosts = std::unique_ptr<float[]>(new float[nsplits]);

    for (int splitDimension = 0; splitDimension < 3; ++splitDimension) {
        // Sort along dimension
        std::sort(begin, end, dimensionwiseIndexedTriangleComparer(triangles, splitDimension));

        // Calculate costs for each splits' left side in forward order
        AABB forwardBox = AABB3f();
        for (ptrdiff_t i = 0; i < nsplits; ++i) {
            const auto &index = begin[i];
            const auto &triangle = triangles[index];
            forwardBox.extend(triangle.aabb());
            const ptrdiff_t nleft = i + 1;
            lcosts[i] = nleft * aabbArea(forwardBox);
        }

        // Calculate AABBs for each split's right side in backward order
        AABB backwardBox = AABB3f();
        std::reverse_iterator<decltype(end)> rbegin(end);
        for (ptrdiff_t i = 0; i < nsplits; ++i) {
            const auto &index = rbegin[i];
            const auto &triangle = triangles[index];
            backwardBox.extend(triangle.aabb());
            const ptrdiff_t nright = i + 1;
            rcosts[i] = nright * aabbArea(backwardBox);
        }

        // Check cost of each split
        for (ptrdiff_t i = 0, rfirst = nsplits - 1; i < nsplits; ++i) {
            const auto &lcost = lcosts[i];
            const auto &rcost = rcosts[rfirst - i];
            const float cost = lcost + rcost;

            if (cost < bestCost) {
                bestCost = cost;
                bestPivot = i + 1;
                bestDimension = splitDimension;
            }
        }
    }

    std::nth_element(begin, begin + bestPivot, end, dimensionwiseIndexedTriangleComparer(triangles, bestDimension));

    return begin + bestPivot;
}

}

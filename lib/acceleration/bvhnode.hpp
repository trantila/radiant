#ifndef BVHNODE_HPP
#define BVHNODE_HPP

#include "triangle.hpp"
#include "aabb.hpp"

#include <memory>


namespace acceleration {

struct BvhNode {
    using AABB = AABB3f;
    using Vec3f = Vector3f;

    struct Intersection {
        float tStart; // = std::numeric_limits<float>::max();
        float tEnd; // = std::numeric_limits<float>::lowest();

        // Whether the intersection is is actually an intersection
        // This assumes t is "limited" to [0, 1] due to having non-unit direction vector
        bool valid() { return tStart <= tEnd && tEnd >= 0.f && tStart <= 1.f; }

        // The actual t value at intersection, taking location of oberserver into account
        float t() { return std::max(tStart, 0.f); } // or  (tStart < 0.f) ? tEnd : tStart;  ?
    };

    AABB box;
    ptrdiff_t first;
    ptrdiff_t last;
    std::unique_ptr<BvhNode> left;
    std::unique_ptr<BvhNode> right;

public:

    BvhNode() : first(-1) {}
    BvhNode(AABB box, ptrdiff_t first, ptrdiff_t last) : box(box), first(first), last(last) {}

    void setChildren(std::unique_ptr<BvhNode> left, std::unique_ptr<BvhNode> right) {
        this->left = std::move(left);
        this->right = std::move(right);
    }

    bool leaf() const { return !left && !right; }
    size_t depth() const;
    size_t descendants() const;

    Intersection intersect(const Vec3f &origin, const Vec3f &directionInverse) const;

    friend std::ostream & operator<< (std::ostream &out, const BvhNode &node);
};

}

#endif // BVHNODE_HPP

#include "bvhnode.hpp"

namespace acceleration {

size_t BvhNode::depth() const {
    if (leaf())
        return 0;
    else
        return std::max(left->depth(), right->depth()) + 1;
}

size_t BvhNode::descendants() const {
    if (leaf())
        return 0;
    else
        return left->descendants() + right->descendants() + 2;
}

BvhNode::Intersection BvhNode::intersect(const Vec3f &origin, const Vec3f &directionInverse) const {
    Array3f t1 = (box.min() - origin).array() * directionInverse.array();
    Array3f t2 = (box.max() - origin).array() * directionInverse.array();
    Array3f tmin = t1.min(t2);
    Array3f tmax = t1.max(t2);
    return BvhNode::Intersection{
        tmin.maxCoeff(),
        tmax.minCoeff(),
    };
}

std::ostream & operator<< (std::ostream &out, const BvhNode &node) {
    out << '[' << node.first << ",\t" << node.last << "]:\t" << node.box.min().transpose() << '\t' << node.box.max().transpose();
    if (node.left && node.right) {
        out << std::endl << *node.left;
        out << std::endl << *node.right;
    }
    else {
        out << std::endl << "... data ...";
    }
    return out;
}

}

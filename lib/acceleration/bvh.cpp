#include "bvh.hpp"

#include "bvhsahsplitter.hpp"

#include <algorithm>
#include <numeric> // accumulate
#include <queue>
#include <stack>
#include <deque>
#include <type_traits>
#include <utility>


#ifndef BVH_MAX_TRIANGLES_IN_LEAF
#define BVH_MAX_TRIANGLES_IN_LEAF 8
#endif // !BVH_MAX_TRIANGLES_IN_LEAF
static_assert(BVH_MAX_TRIANGLES_IN_LEAF > 0, "Must be at least 1 triangle per leaf!");


namespace {
using namespace acceleration;

std::vector<size_t> createRange(size_t n) {
    std::vector<size_t> indices;
    indices.reserve(n);
    for (size_t i = 0; i < n; ++i) indices.push_back(i);
    return indices;
}

template <typename T>
T popTop(std::stack<T> &stack) {
    T top = stack.top();
    stack.pop();
    return top;
}

AABB3f boundingBoxOf(
    const std::vector<Triangle> &triangles,
    Bvh::IndexIterator begin,
    Bvh::IndexIterator end,
    AABB3f initialBox
) {
    return std::accumulate(begin, end, initialBox, [&triangles](AABB3f box, size_t i) {
        const auto &triangle = triangles[i];
        return box.merged(triangle.aabb());
    });
}

Bvh::Splitter getSplitter(SplitMode splitmode, const std::vector<Triangle> &triangles) {
    switch (splitmode)
    {
    case SplitMode_None:
        // This is only for interfacecompatibility; SplitMode of None implies depth of 1 in reality
        return [](const BvhNode::AABB &, Bvh::IndexIterator, Bvh::IndexIterator) {
            return Bvh::IndexIterator();
        };
    case SplitMode_ObjectMedian:
        return [&triangles](const BvhNode::AABB &box, Bvh::IndexIterator begin, Bvh::IndexIterator end) {
            int splitDimension = longestDimension(box);

            ptrdiff_t n = end - begin;
            auto pivot = begin + n / 2;
            std::partial_sort(begin, pivot, end, [&triangles, splitDimension](size_t l, size_t r) {
                auto lc = triangles[l].centroid();
                auto rc = triangles[r].centroid();
                return lc[splitDimension] < rc[splitDimension];
            });

            return pivot;
        };
    case SplitMode_SpatialMedian:
        return [&triangles](const BvhNode::AABB &box, Bvh::IndexIterator begin, Bvh::IndexIterator end) {
            Vector3f span = box.max() - box.min();
            int splitDimension = longestDimension(span);
            float splitValue = box.min()[splitDimension] + 0.5f * span[splitDimension];

            auto pivot = std::partition(begin, end, [&triangles, splitDimension, splitValue](size_t i) {
                return triangles[i].centroid()[splitDimension] < splitValue;
            });

            return pivot;
        };
    case SplitMode::SplitMode_Sah:
        return BvhSahSplitter(triangles);
    }
    //default:
    throw std::runtime_error("Splitmode does not exist or does not have a handler");
}


template <
        typename T,
        typename = std::enable_if_t<std::is_scalar_v<T> || std::is_array_v<T>>>
void write(std::ostream &out, const T &value) {
    out.write(reinterpret_cast<const char *>(&value), sizeof(value));
}

template <
        typename T,
        typename = std::enable_if_t<std::is_scalar_v<T>>>
void write(std::ostream &out, const T *data, size_t n) {
    size_t nbytes = sizeof(*data)*n;
    out.write(reinterpret_cast<const char *>(data), static_cast<std::streamsize>(nbytes));
}

template <
        typename T,
        typename = std::enable_if_t<std::is_scalar_v<T> || std::is_array_v<T>>>
void read(std::istream &in, T &value) {
    in.read(reinterpret_cast<char *>(&value), sizeof(value));
}

template <
        typename T,
        typename = std::enable_if_t<std::is_scalar_v<T>>>
void read(std::istream &in, T *data, size_t n) {
    size_t nbytes = sizeof(*data)*n;
    in.read(reinterpret_cast<char *>(data), static_cast<std::streamsize>(nbytes));
}

// IO support for Eigen::Matrix

template <typename T, int R, int C>
void write(std::ostream &out, const Eigen::Matrix<T, R, C> &mat) {
    ptrdiff_t n = mat.rows() * mat.cols();
    write(out, mat.data(), static_cast<size_t>(n));
}
template <typename T, int R, int C>
void read(std::istream &in, Eigen::Matrix<T, R, C> &mat) {
    ptrdiff_t n = mat.rows() * mat.cols();
    read(in, mat.data(), static_cast<size_t>(n));
}

// IO support for BvhNode

void write(std::ostream &out, const BvhNode &node) {
    write(out, node.first);
    write(out, node.last);
    write(out, node.box.min());
    write(out, node.box.max());
}
void read(std::istream &in, BvhNode &node) {
    read(in, node.first);
    read(in, node.last);
    read(in, node.box.min());
    read(in, node.box.max());
}

}

namespace acceleration {

Bvh::Bvh(const std::vector<Triangle> &triangles, SplitMode splitmode, unsigned maxdepth)
        : Bvh() {
    size_t n = triangles.size();
    indices = createRange(n);

    auto splitter = getSplitter(splitmode, triangles);
    if (splitmode == SplitMode_None) maxdepth = 0;

    auto begin = indices.begin();
    auto end = indices.end();

    AABB box = boundingBoxOf(triangles, begin, end, AABB());
    root = BvhNode(box, 0, static_cast<ptrdiff_t>(n));

    std::deque<BvhNode *> nodeBatchQueue{ &root };
    while (!nodeBatchQueue.empty()) {
        nodeCount += nodeBatchQueue.size();
        treeDepth += 1u;

        if (treeDepth >= maxdepth) break;

        auto nodeCountOnDepth = static_cast<ptrdiff_t>(nodeBatchQueue.size());

        #pragma omp parallel for
        for (ptrdiff_t i = 0; i < nodeCountOnDepth; ++i) {
            auto &node = *nodeBatchQueue[static_cast<size_t>(i)];
            auto nodeN = node.last - node.first;

            if (nodeN <= BVH_MAX_TRIANGLES_IN_LEAF) continue;

            auto nodeBegin = begin + node.first;
            auto nodeEnd = begin + node.last;

            auto nodePivot = splitter(node.box, nodeBegin, nodeEnd);
            auto nodeMiddle = node.first + (nodePivot - nodeBegin); // begin + nodePivot ?

            // Resolve degenerate cases in a crude manner
            if (nodeMiddle == node.first) ++nodeMiddle;
            else if (nodeMiddle == node.last) --nodeMiddle;

            AABB lbox = boundingBoxOf(triangles, nodeBegin, nodePivot, AABB());
            auto left = std::unique_ptr<BvhNode>(new BvhNode(lbox, node.first, nodeMiddle));
            AABB rbox = boundingBoxOf(triangles, nodePivot, nodeEnd, AABB());
            auto right = std::unique_ptr<BvhNode>(new BvhNode(rbox, nodeMiddle, node.last));

            #pragma omp critical
            {
                nodeBatchQueue.push_back(left.get());
                nodeBatchQueue.push_back(right.get());
            }

            node.setChildren(std::move(left), std::move(right));
        }
        auto begin = nodeBatchQueue.begin();
        nodeBatchQueue.erase(begin, begin + nodeCountOnDepth);
    }
}

#ifdef _OPENMP
#define RADIANT_THREAD_LOCAL
#else
#define RADIANT_THREAD_LOCAL thread_local
#endif


float Bvh::query(const Vec3f &origin, const Vec3f &direction, IntersectionHandler callback) const {
    // NOTE Turns out that allocating N times per query (by std::stack) really
    //      weighs on performance so instead a thread-static semi-dynamic stack
    //      is used here.

    RADIANT_THREAD_LOCAL static size_t treeDepthOfStack = 0;
    #pragma omp threadprivate(treeDepthOfStack)
    //static std::unique_ptr<const BvhNode *[]> stack; // No-go for VS...
    RADIANT_THREAD_LOCAL static const BvhNode **stack = nullptr;
    #pragma omp threadprivate(stack)

    // Ensure a good stack. Should likely branch-predict quite well...
    size_t treeDepth = depth();
    if (treeDepthOfStack != treeDepth) {
        // NOTE stack is only deleted when stack size changes, i.e. this sort-of leaks memory
        //stack = std::make_unique<const BvhNode *[]>(treeDepthOfStack + 1);
        delete[] stack;
        stack = new const BvhNode *[treeDepth + 1];
        treeDepthOfStack = treeDepth;
    }

    // NOTE Alternatively a truly static stack is an even more rigid and unsafe
    //      yet more performant option.
    //const BvhNode *stack[32];

    Vec3f directionInverse = direction.cwiseInverse();

    size_t sp = 0;
    stack[sp] = &root;

    float tmin = 1.f;
    while (sp != static_cast<size_t>(-1)) {
        auto node = stack[sp--];
        auto intersection = node->intersect(origin, directionInverse);
        if (intersection.valid() && intersection.t() < tmin) {
            if (!node->leaf()) {
                stack[++sp] = node->right.get();
                stack[++sp] = node->left.get();
            }
            else {
                tmin = callback(Bvh::ElementGrouping{
                    indices.begin() + node->first,
                    indices.begin() + node->last,
                });
            }
        }
    }

    return tmin;
}

void Bvh::save(std::ostream &out) {
    size_t ntriangles = indices.size();

    // Stream out triangle indices
    write(out, ntriangles);
    write(out, indices.data(), ntriangles);

    // Stream out the hierarchy
    write(out, size());

    std::queue<BvhNode *> nodeQueue;
    nodeQueue.push(&root);
    while (!nodeQueue.empty()) {
        auto node = nodeQueue.front();
        nodeQueue.pop();

        // Stream out the node (limiting indices and bounding box)
        write(out, *node);

        // Enqueue child nodes if found, leaving left on top
        if (!node->leaf()) {
            nodeQueue.push(node->left.get());
            nodeQueue.push(node->right.get());
        }
    }
}

void Bvh::load(std::istream &in) {
    size_t ntriangles;
    read(in, ntriangles);

    // Stream in triangle indices
    indices = std::vector<size_t>(ntriangles);
    read(in, indices.data(), ntriangles);

    // Stream in the hierarchy
    size_t nnodes;
    read(in, nnodes);

    if (nnodes % 2 == 0) throw std::runtime_error("Number of BVH nodes should be odd");

    read(in, root);
    nodeCount = 1;
    treeDepth = 1;

    std::queue<BvhNode *> nodeQueue;
    nodeQueue.push(&root);
    BvhNode *node = nodeQueue.front();
    while (nodeCount < nnodes) {
        auto left = std::unique_ptr<BvhNode>(new BvhNode());
        auto right = std::unique_ptr<BvhNode>(new BvhNode());

        read(in, *left);
        read(in, *right);

        // If the current node is not the parent of the loaded nodes, scan until one is found.
        // This is highly relevant since not all branches may go to maximum depth.
        while (node->first != left->first || node->last != right->last) {
            auto previousFirst = node->first;

            nodeQueue.pop();
            if (nodeQueue.empty()) throw std::runtime_error("Indices of loaded BVH nodes do not match those higher in the tree");
            node = nodeQueue.front();

            // If the first index has gone down, we're on a new depth
            if (node->first < previousFirst) ++treeDepth;
        }

        nodeQueue.push(left.get());
        nodeQueue.push(right.get());

        node->left = std::move(left);
        node->right = std::move(right);

        nodeCount += 2;
    }
}

}

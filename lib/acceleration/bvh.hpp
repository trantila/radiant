#ifndef BVH_HPP
#define BVH_HPP

#include "bvhnode.hpp"
#include "triangle.hpp"

#include <ostream>
#include <istream>
#include <vector>
#include <functional>


namespace acceleration {

enum SplitMode {
    /// It would be much better to define a ISpatialAccelerator or the like, since the BVH
    /// accesses by indices (extra indirection). But this serves as a comparison for now.
    SplitMode_None = 0x00,
    SplitMode_ObjectMedian = 0x01,
    SplitMode_SpatialMedian = 0x02,
    SplitMode_Sah = 0x04,
};

class Bvh {
public:
    using Vec3f = Vector3f;
    using AABB = AABB3f;
    using IndexIterator = std::vector<size_t>::iterator;
    using SplitterSig = IndexIterator (const AABB &box, IndexIterator begin, IndexIterator end);
    using Splitter = std::function<SplitterSig>;

    struct ElementGrouping {
        std::vector<size_t>::const_iterator indicesBegin;
        std::vector<size_t>::const_iterator indicesEnd;
    };

    using IntersectionHandlerSig = float (Bvh::ElementGrouping);
    using IntersectionHandler = std::function<IntersectionHandlerSig>;

private:
    std::vector<size_t> indices;
    BvhNode root;

    // "Cached" and bound to break if tree mutations are ever implemented
    size_t treeDepth;
    size_t nodeCount;

public:
    Bvh() : treeDepth(0), nodeCount(0) {}
    Bvh(const std::vector<Triangle> &triangles, SplitMode splitmode = SplitMode_Sah, unsigned maxdepth = -1u);

    size_t depth() const { return treeDepth; }
    size_t size() const { return nodeCount; }

    float query(const Vec3f &origin, const Vec3f &direction, IntersectionHandler callback) const;

    void save(std::ostream &out);
    void load(std::istream &in);

    friend std::ostream & operator<< (std::ostream &out, const Bvh &bvh) { return out << bvh.root; }
};

}

#endif // BVH_HPP

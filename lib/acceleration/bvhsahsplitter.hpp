#ifndef BVHSAHSPLITTER_HPP
#define BVHSAHSPLITTER_HPP

#include "bvh.hpp"
#include "triangle.hpp"

#include <vector>


namespace acceleration {

class BvhSahSplitter
{
public:
    using IndexIterator = Bvh::IndexIterator;
    using AABB = Bvh::AABB;

private:
    const std::vector<Triangle> &triangles;

public:
    BvhSahSplitter(const std::vector<Triangle> &triangles) : triangles(triangles) {}

    IndexIterator operator ()(const AABB &box, IndexIterator begin, IndexIterator end);
};

}

#endif // BVHSAHSPLITTER_HPP

#ifndef IMAGE_HPP
#define IMAGE_HPP

#include <Eigen/Eigen>

#include <sstream>
#include <stdexcept>


class Image {
public:
    using Buffer = Eigen::Matrix4Xf;
    using BlockRef = Buffer::BlockXpr;
    using ConstBlockRef = Buffer::ConstBlockXpr;
    using RowRef = BlockRef; //Buffer::BlockXpr;
    using ConstRowRef = ConstBlockRef; //Buffer::ConstBlockXpr;
    using Pixel = Eigen::Vector4f;
    using PixelRef = Buffer::ColXpr;
    using ConstPixelRef = Buffer::ConstColXpr;

private:
    unsigned _width;
    Buffer _buffer;

    ptrdiff_t toBufferIndex(unsigned row, unsigned col) const { return row * _width + col; }

public:
    Image(unsigned width, unsigned height)
        : _width(width)
        , _buffer(Buffer::Zero(Buffer::RowsAtCompileTime, width * height)) {}

    Image(unsigned width, unsigned height, Buffer buffer)
        : _width(width)
        , _buffer(buffer) {
        if (width * height != buffer.cols()) {
            std::stringstream msg;
            msg << "Image size " << width << "x" << height << "(=" << width * height << ") "
                << "does not match size of given buffer (" << buffer.cols() << ")";
            throw std::logic_error(msg.str());
        }
    }

    unsigned width() const { return _width; }
    unsigned height() const { return static_cast<unsigned>(_buffer.cols() / _width); }

    ConstBlockRef buffer() const { return _buffer.block(0, 0, Buffer::RowsAtCompileTime, _buffer.cols()); }
    BlockRef buffer() { return _buffer.block(0, 0, Buffer::RowsAtCompileTime, _buffer.cols()); }

    ConstRowRef row(unsigned n) const {
        return _buffer.block(0, n * _width, Buffer::RowsAtCompileTime, _width);
    }
    RowRef row(unsigned n) {
        return _buffer.block(0, n * _width, Buffer::RowsAtCompileTime, _width);
    }

    ConstPixelRef pixel(unsigned row, unsigned col) const {
        auto idx = toBufferIndex(row, col);
        return _buffer.col(idx);
    }
    PixelRef pixel(unsigned row, unsigned col) {
        auto idx = toBufferIndex(row, col);
        return _buffer.col(idx);
    }
};

#endif // IMAGE_HPP

#ifndef TRIANGLEINTERSECTION_HPP
#define TRIANGLEINTERSECTION_HPP

#include "triangle.hpp"
#include "math.hpp"


struct TriangleIntersection
{
    float t, u, v;
public:
    TriangleIntersection() : t(inf()), u(inf()), v(inf()) {}

    bool valid(float maxdistance = 1.f, float mindistance = 0.000001f) {
        // Turst blindly that u, v are ok if t is good.
        return t >= mindistance && t < maxdistance;
    }
};


TriangleIntersection mollerTrumboreIntersect(
        const Vector3f &rayOrigin,
        const Vector3f &rayDir,
        const Triangle &triangle,
        float epsilon = 0.0000001f);


#endif // TRIANGLEINTERSECTION_HPP

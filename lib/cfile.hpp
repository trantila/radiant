#ifndef CFILE_HPP
#define CFILE_HPP

#include <string>
#include <functional>
#include <memory>
#include <cstdio>


using unique_file_ptr = std::unique_ptr<FILE, std::function<void (FILE *)>>;

unique_file_ptr open_file(const std::string &filename, const std::string &mode);


#endif // CFILE_HPP

#ifndef AABB_HPP
#define AABB_HPP

#include "math.hpp"

#include <Eigen/Geometry>

// Turns out Eigen already has this with pretty much all the sweeties. : )
// Most notably the Eigen defines "emptiness" a bit losely as any "negative" space.
using AABB3f = Eigen::AlignedBox3f;

inline float aabbArea(const AABB3f& box) {
    Vector3f d(box.max() - box.min());
    return 2 * (d.x() * d.y() + d.x() * d.z() + d.y() * d.z());
}

inline int longestDimension(const AABB3f &box) {
    Vector3f d(box.max() - box.min());
    return longestDimension(d);
}

//struct AABB3f {
//    Vector3f min;
//    Vector3f max;

//    static AABB Create(Vector3f min, Vector3f max) { return AABB { min, max }; }
//    static AABB Empty() { return Create(Eigen3f::Zero(), Eigen3f::Zero()); }
//    static AABB Void() {
//        float inf = inf();
//        float ninf = -inf;
//        return Create(Vector3f(inf, inf, inf), Vector3f(ninf, ninf, ninf));
//    }
//}

#endif // AABB_HPP

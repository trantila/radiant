#ifndef MATH_HPP
#define MATH_HPP

// Basic linear algebra
#include <Eigen/Core>
// Complete Eigen-provided "advanced" linear algebra
//#include <Eigen/Dense>

#include <type_traits>
#include <limits>
#include <cmath>


using Eigen::Vector3f;
using Eigen::Array3f;
using Eigen::Matrix3f;
using Eigen::Vector4f;
using Eigen::Array4f;
using Eigen::Matrix4f;
using Eigen::Vector3i;
using Eigen::Array3i;
using Eigen::Vector4i;
using Eigen::Array4i;


template <typename T, int R>
int longestDimension(const Eigen::Matrix<T, R, 1> &vec) {
    int longestDim = 0;
    float longest = vec[0];
    for (int dim = 1; dim < vec.rows(); ++dim) {
        auto candidate = vec[dim];
        if (candidate > longest) {
            longest = candidate;
            longestDim = dim;
        }
    }
    return longestDim;
}

template <typename T = float, typename = std::enable_if_t<std::is_floating_point_v<T>>>
constexpr T g_pi = T(3.1415926535897932385L);
template <typename T = float, typename = std::enable_if_t<std::is_floating_point_v<T>>>
constexpr T g_inf = std::numeric_limits<T>::infinity();

template <typename T = float, typename = std::enable_if_t<std::is_floating_point_v<T>>>
constexpr T pi() { return g_pi<T>; }
template <typename T = float, typename = std::enable_if_t<std::is_floating_point_v<T>>>
constexpr T inf() { return g_inf<T>; }


template <
        typename TNumerator,
        typename TDenominator,
        typename TResult = float,
        std::enable_if_t<std::is_arithmetic_v<TNumerator>, int> = 0,
        std::enable_if_t<std::is_arithmetic_v<TDenominator>, int> = 0,
        std::enable_if_t<std::is_floating_point_v<TResult>, int> = 0
>
TResult ratio(TNumerator num, TDenominator den) {
    return static_cast<TResult>(num) / static_cast<TResult>(den);
}

#endif // MATH_HPP

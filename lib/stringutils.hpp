#ifndef STRINGUTILS_H
#define STRINGUTILS_H

#include "streamutils.hpp"

#include <sstream>
#include <string>


/// Convert iteratees to strings (through <<) and catenate with given separators.
template<typename TIter>
std::string join(TIter begin, TIter end, const std::string& separator = "") {
    std::stringstream stringbuilder;
    for (auto iter = begin; iter < end; ++iter) {
        if (iter == begin) stringbuilder << *iter;
        else               stringbuilder << separator << *iter;
    }
    return stringbuilder.str();
}

/// Combine params to a string through a stringstream
template <typename... TTypes>
std::string ss(TTypes... values) {
    std::stringstream stringbuilder;
    stream(stringbuilder, values...);
    return stringbuilder.str();
}


#endif // STRINGUTILS_H

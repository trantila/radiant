#include "cfile.hpp"

#include "stringutils.hpp"

#include <stdexcept>


unique_file_ptr open_file(const std::string &filename, const std::string &mode) {
    unique_file_ptr file{ std::fopen(filename.c_str(), mode.c_str()), [](FILE *file) { std::fclose(file); } };
    if (!file)
        throw std::logic_error(ss("File '", filename, "' could not be opened in mode '", mode, '\''));
    return file;
}

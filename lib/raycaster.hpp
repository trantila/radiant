#ifndef RAYCASTER_HPP
#define RAYCASTER_HPP

#include "triangle.hpp"
#include "math.hpp"
#include "bvh.hpp"

#include <vector>


class RayCaster
{
public:
    struct Result {
        const Triangle *triangle;
        float t, u, v;
        Vector3f point;

        Result() : triangle(nullptr), t(inf()), u(inf()), v(inf()) {}

        bool hit() const { return triangle != nullptr; }
    };

private:
    const std::vector<Triangle> &triangles;
    const acceleration::Bvh &lookup;
    float epsilon;

public:
    RayCaster(const std::vector<Triangle> &triangles, const acceleration::Bvh &lookup, float epsilon = 0.000001f)
        : triangles(triangles), lookup(lookup), epsilon(epsilon) {}

    Result cast(const Vector3f &rayOrigin, const Vector3f &rayDir) const;
};

#endif // RAYCASTER_HPP

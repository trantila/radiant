#ifndef MATERIAL_HPP
#define MATERIAL_HPP

#include "math.hpp"


struct Material {
    Vector3f diffuse;
    Vector3f specular;
};

#endif // MATERIAL_HPP

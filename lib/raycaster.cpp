#include "raycaster.hpp"

#include "triangleintersection.hpp"


void fillResult(RayCaster::Result &result, const Triangle &triangle, const TriangleIntersection &intersection) {
    result.triangle = &triangle;
    result.t = intersection.t;
    result.u = intersection.u;
    result.v = intersection.v;
}

RayCaster::Result RayCaster::cast(const Vector3f &rayOrigin, const Vector3f &rayVector) const
{
    Result result{};

    lookup.query(rayOrigin, rayVector, [&](acceleration::Bvh::ElementGrouping group) {
        // std::transform_reduce not available as of today :(
        for (auto indexIter = group.indicesBegin; indexIter != group.indicesEnd; ++indexIter) {
            const auto &triangle = triangles[*indexIter];
            auto intersection = mollerTrumboreIntersect(rayOrigin, rayVector, triangle, epsilon);

            if (intersection.t >= epsilon && intersection.t < result.t)
                fillResult(result, triangle, intersection);
        }
        return result.t;
    });

    result.point = rayOrigin + rayVector * result.t;

    return result;
}

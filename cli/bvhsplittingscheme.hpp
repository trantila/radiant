#ifndef BVHBUILDERTYPE_HPP
#define BVHBUILDERTYPE_HPP

#include "bvh.hpp"
#include "enumstringconversions.hpp"

#include <stdexcept>


enum class BVHSplittingScheme {
    None = 0,
    ObjectMedian = 1,
    SpatialMedian = 2,
    SurfaceAreaHeuristic = 4,
};

template<>
inline std::vector<KVP<BVHSplittingScheme>> EnumStringConversions<BVHSplittingScheme>::Mappings = {
    {"none", BVHSplittingScheme::None},
    {"object-median", BVHSplittingScheme::ObjectMedian},
    {"spatial-median", BVHSplittingScheme::SpatialMedian},
    {"sah", BVHSplittingScheme::SurfaceAreaHeuristic},
};

acceleration::SplitMode getBVHSplitMode(BVHSplittingScheme scheme) {
    switch (scheme) {
    case BVHSplittingScheme::None:
        return acceleration::SplitMode_None;
    case BVHSplittingScheme::ObjectMedian:
        return acceleration::SplitMode_ObjectMedian;
    case BVHSplittingScheme::SpatialMedian:
        return acceleration::SplitMode_SpatialMedian;
    case BVHSplittingScheme::SurfaceAreaHeuristic:
        return acceleration::SplitMode_Sah;
    }
    throw std::runtime_error("No domain-conversion defined for given splitting scheme");
}

#endif // BVHBUILDERTYPE_HPP

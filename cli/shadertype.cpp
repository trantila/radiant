#include "shadertype.hpp"

#include "headlightshader.hpp"
#include "depthshader.hpp"
#include "stringutils.hpp"

#include <stdexcept>


std::unique_ptr<IShader> getShader(ShaderType shaderType, float cutOff) {
    // NOTE This cutOff seems off-place here; maybe should not bundle shader
    //      creation at all since their params may vary wildly..?
    switch (shaderType) {
    case ShaderType::Depth:
        return std::make_unique<DepthShader>(cutOff);
    case ShaderType::HeadLight:
        return std::make_unique<HeadlightShader>();
    }
    //default:
    throw std::logic_error(ss("Initialization of shader is not defined for `", shaderType, '`'));
}

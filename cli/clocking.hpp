#ifndef CLOCKING_HPP
#define CLOCKING_HPP

#include <ostream>
#include <string>
#include <chrono>


class StopWatch {
    using Clock = std::chrono::high_resolution_clock;

    Clock::time_point tstart;

    std::chrono::duration<double, std::milli> tdelta() {
        Clock::time_point tend = Clock::now();
        return tend - tstart;
    }

public:
    StopWatch() : tstart(Clock::now()) {}

    double check() {
        return tdelta().count();
    }

    double reset() {
        auto dt = tdelta();
        tstart = Clock::now();
        return dt.count();
    }
};

class AutoStopWatch final {
    StopWatch stopwatch;
    std::string label;
    std::ostream &out;
public:
    AutoStopWatch(std::string label = "time");
    AutoStopWatch(std::ostream &out, std::string label = "time");
    ~AutoStopWatch();
};

#endif // CLOCKING_HPP

#ifndef ENUMSTRINGCONVERSIONS_HPP
#define ENUMSTRINGCONVERSIONS_HPP

#include "stringutils.hpp"

#include <istream>
#include <ostream>
#include <vector>
#include <string>
#include <algorithm>
#include <type_traits>


template <typename T>
using KVP = std::pair<const std::string, const T>;


//template <typename TEnum>
//extern std::vector<KVP<TEnum>> enumStringMappings;

template <typename TEnum>
struct EnumStringConversions {
    static std::vector<KVP<TEnum>> Mappings;
};



template <
        typename TEnum,
        typename std::enable_if_t<std::is_enum_v<TEnum>, bool> = true>
std::istream & operator >> (std::istream &in, TEnum &shaderType) {
    std::string token;
    in >> token;
    auto endIter = std::end(EnumStringConversions<TEnum>::Mappings);
    auto iter = std::find_if(std::begin(EnumStringConversions<TEnum>::Mappings), endIter, [&token](const KVP<TEnum> &kvp) {
        return kvp.first == token;
    });
    if (iter == endIter)
        throw std::runtime_error(ss("Unknown shader type `", token, '`'));
    shaderType = iter->second;
    return in;
}

template <
        typename TEnum,
        typename std::enable_if_t<std::is_enum_v<TEnum>, bool> = true>
std::ostream & operator << (std::ostream &out, TEnum shaderType) {
    auto endIter = std::end(EnumStringConversions<TEnum>::Mappings);
    auto iter = std::find_if(std::begin(EnumStringConversions<TEnum>::Mappings), endIter, [shaderType](const KVP<TEnum> &kvp) {
        return kvp.second == shaderType;
    });
    if (iter == endIter) {
        auto shaderTypeAsInt = static_cast<std::underlying_type_t<TEnum>>(shaderType);
        throw std::logic_error(ss("No string representation found for shaderType `", shaderTypeAsInt,'`'));
    }
    return out << iter->first;
}

#endif // ENUMSTRINGCONVERSIONS_HPP

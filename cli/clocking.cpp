#include "clocking.hpp"

#include <iostream>


AutoStopWatch::AutoStopWatch(std::string label) : AutoStopWatch(std::cerr, label) {}

AutoStopWatch::AutoStopWatch(std::ostream &out, std::string label) : stopwatch(), label(label), out(out) {}

AutoStopWatch::~AutoStopWatch() {
    auto precisionBefore = out.precision();
    out.precision(6);
    out << label << ": " << stopwatch.check() << "ms" << std::endl;
    out.precision(precisionBefore);
}

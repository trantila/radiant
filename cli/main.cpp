#include "content/aisceneio.hpp"
#include "pngio.hpp"
#include "image.hpp"
#include "modelbuilding.hpp"
#include "depthshader.hpp"
#include "headlightshader.hpp"
#include "renderer.hpp"
#include "raycaster.hpp"
#include "triangle.hpp"
#include "bvhsplittingscheme.hpp"
#include "shadertype.hpp"
#include "stringutils.hpp"
#include "cfile.hpp"
#include "clocking.hpp"

#include <args.hxx>

#include <filesystem>
#include <iostream>
#include <fstream>
#include <stdexcept>
#include <string>
#include <optional>
#include <numeric>
#include <algorithm>
#include <memory>


using namespace args;
using namespace content;


template <typename TOption>
auto getAndLogArg(const std::string &header, TOption &arg) {
    auto value = args::get(arg);
    std::cerr << header << ":\t" << value << std::endl;
    return value;
}

template <typename TOption>
auto getAndLogOptionalArg(const std::string &header, TOption &arg, const std::string &fallback = "-") {
    using T = std::remove_reference_t<decltype(args::get(arg))>;
    std::optional<T> maybeValue;
    if (arg) maybeValue = args::get(arg);
    std::cerr << header << ":\t" << (arg ? *maybeValue : fallback) << std::endl;
    return maybeValue;
}

aiSceneConstUniquePtr loadLoggedNullableAssimpScene(const std::string &filepath) {
    aiSceneConstUniquePtr scene = nullptr;
    try {
        scene = loadAssimpScene(filepath);
    } catch (const aiSceneLoadingError &error) {
        std::cerr << error.what() << std::endl;
    }
    return scene;
}

void checkAndNotifyAboutAssimpSceneValidationWarnings(const aiScene &scene) {
    if (scene.mFlags & AI_SCENE_FLAGS_VALIDATION_WARNING) {
        // TODO Where that log at?
        std::cerr << "Assimp detected something fishy in the imported scene, check log for details!" << std::endl;
    }
}


int main(int argc, char* argv[]) {
    StopWatch stopwatch{};

    ArgumentParser parser("'Race dem rays", "... Good tracin'!");
    parser.helpParams.addDefault = true;
    parser.helpParams.addChoices = true;
    HelpFlag helpFlag(parser, "help", "Display this help listing", {'h', "help"});
    ValueFlag<std::string> outArg(parser, "filename", "Output destination, defaults to stdout", {'o', "out"});
    Positional<std::filesystem::path> fileArg(parser, "file", "Input model file", Options::Required);

    Group accelerationArgGroup(parser, "Acceleration parameters", Group::Validators::DontCare);
    ValueFlag<BVHSplittingScheme> splitSchemeArg(
                accelerationArgGroup, "splitting", "Splitting scheme for accelerator", {"splitting"}, BVHSplittingScheme::SurfaceAreaHeuristic);
    Flag forceSplittingFlag(accelerationArgGroup, "Force splitting", "Force re-splitting even if a cached lookup was found", {"force-splitting"});

    Group raycastArgGroup(parser, "Ray-casting parameters", Group::Validators::DontCare);
    ValueFlag<float> cutOffArg(raycastArgGroup, "cutoff", "Cut-off distance of rays", {"cut-off"}, 5.f);

    Group renderArgGroup(parser, "Rendering parameters", Group::Validators::DontCare);
    ValueFlag<size_t> widthArg(renderArgGroup, "width", "Width of the produced image", {'w', "width"}, 400);
    ValueFlag<size_t> heightArg(renderArgGroup, "height", "Height of the produced image", {'h', "height"}, 300);
    ValueFlag<float> fovArg(renderArgGroup, "fov", "Field-of-view of the renderer", {"fov"}, pi() / 2.f);
    ValueFlag<float> epsilonArg(renderArgGroup, "epsilon", "Epsilon used in various floating point computations", {"epsilon"}, 0.000001f);
    ValueFlag<ShaderType> shaderTypeArg(renderArgGroup, "shader", "Shader used for rendering", {"shader"}, ShaderType::HeadLight);

    try {
        std::cerr << std::boolalpha;
        parser.ParseCLI(argc, argv);
    }
    catch (const Help &) {
        std::cerr << parser;
        return EXIT_SUCCESS;
    }
    catch (const args::Error &e) {
        std::cerr << e.what() << std::endl;
        std::cerr << parser;
        return EXIT_FAILURE;
    }
    catch (...) {
        std::cerr << "Something incredible and flaky happened!" << std::endl;
        return EXIT_FAILURE;
    }

    const auto filename = getAndLogArg("Input", fileArg);
    const auto outfilename = getAndLogOptionalArg("Output", outArg);
    const auto splitScheme = getAndLogArg("Splitting", splitSchemeArg);
    const auto forceSplitting = getAndLogArg("Force re-splitting", forceSplittingFlag);
    const auto cutOff = getAndLogArg("Cut-off", cutOffArg);
    const auto shaderType = getAndLogArg("Shading", shaderTypeArg);
    const auto width = getAndLogArg("Width", widthArg);
    const auto height = getAndLogArg("Height", heightArg);
    const auto fov = getAndLogArg("Field-of-view", fovArg);
    const auto epsilon = getAndLogArg("Epsilon", epsilonArg);

    try {
        std::cerr << "Loading scene..." << std::endl;
        stopwatch.reset();

        auto aiScene = loadLoggedNullableAssimpScene(filename.string());
        checkAndNotifyAboutAssimpSceneValidationWarnings(*aiScene);
        auto model = buildModel(*aiScene, &std::cerr);

        std::cerr << "Loaded scene in " << stopwatch.reset() << "ms" << std::endl;

        auto &triangles = model.triangles();

        std::cerr << "Triangles:\t" << triangles.size() << std::endl;

        auto aabb = std::accumulate(triangles.begin(), triangles.end(), AABB3f(), [](AABB3f aabb, const Triangle &triangle) {
            return aabb.extend(triangle.aabb());
        });

        std::cerr << "AABB:\t((" << aabb.min().transpose() << "), (" << aabb.max().transpose() << "))" << std::endl;

        // Apparently people like having xz as ground and y as up..
        Vector3f lookFrom = aabb.corner(AABB3f::CornerType::TopLeftFloor);
        Vector3f lookTo = aabb.corner(AABB3f::CornerType::BottomRightCeil);
        Vector3f viewDir = (lookTo - lookFrom).normalized();
        Vector3f viewPos = lookFrom + viewDir * 0.01f;
        Vector3f dirOnGroundPlane = viewDir.cross(Vector3f::UnitY());
        Vector3f viewUpDir = dirOnGroundPlane.cross(viewDir);

        Renderer::Camera4f camera;
        camera.col(0) = dirOnGroundPlane.homogeneous();
        camera.col(1) = (-viewUpDir).homogeneous();
        camera.col(2) = viewDir.homogeneous();
        camera.col(3) = viewPos.homogeneous();

        std::cerr << "View o:\t" << viewPos.transpose() << std::endl;
        std::cerr << "View d:\t" << viewDir.transpose() << std::endl;
        std::cerr << "View u:\t" << viewUpDir.transpose() << std::endl;

        Image img(width, height, Image::Buffer(4, width * height));

        auto shader = getShader(shaderType, cutOff);
        auto splitMode = getBVHSplitMode(splitScheme);

        auto lookupCacheFilepath = filename;
        lookupCacheFilepath.replace_extension(ss(".", splitScheme, ".bvh"));

        acceleration::Bvh lookup;
        if (!forceSplitting && std::filesystem::exists(lookupCacheFilepath)) {
            std::cerr << "Loading a look-up from " << lookupCacheFilepath << "..." << std::endl;
            std::ifstream lookupCacheInStream{lookupCacheFilepath};
            stopwatch.reset();
            lookup.load(lookupCacheInStream);
            std::cerr << "Loaded a look-up in " << stopwatch.reset() << "ms" << std::endl;
        } else {
            std::cerr << "Constructing a look-up..." << std::endl;
            stopwatch.reset();
            lookup = acceleration::Bvh(triangles, splitMode);
            std::cerr << "Constructed a look-up in " << stopwatch.reset() << "ms" << std::endl;

            std::cerr << "Saving the built look-up to " << lookupCacheFilepath << "..." << std::endl;
            std::ofstream lookupCacheOutStream{lookupCacheFilepath};
            stopwatch.reset();
            lookup.save(lookupCacheOutStream);
            std::cerr << "Saved the built in look-up in " << stopwatch.reset() << "ms" << std::endl;
        }

        RayCaster rayCaster(triangles, lookup, epsilon);
        Renderer renderer(rayCaster, cutOff);

        std::cerr << "Rendering..." << std::endl;
        stopwatch.reset();
        renderer.render(img, camera, fov, *shader);
        std::cerr << "Rendered in " << stopwatch.reset() << "ms" << std::endl;

        std::cerr << "Writing..." << std::endl;
        auto file = outfilename.has_value() ? open_file(*outfilename, "wb") : nullptr;
        writeAsPng(img, file.get());
    }
    catch (const std::exception &e) {
        std::cerr << e.what() << std::endl;
        return EXIT_FAILURE;
    }
    catch (...) {
        std::cerr << "BURN, BABY, BURN!" << std::endl;
        return EXIT_FAILURE;
    }

    return EXIT_SUCCESS;
}

#ifndef SHADERTYPE_HPP
#define SHADERTYPE_HPP

#include "ishader.hpp"
#include "enumstringconversions.hpp"

#include <istream>
#include <ostream>
#include <memory>


enum class ShaderType
{
    Depth = 0,
    HeadLight,
};


template <>
inline std::vector<KVP<ShaderType>> EnumStringConversions<ShaderType>::Mappings = {
    {"depth", ShaderType::Depth},
    {"headlight", ShaderType::HeadLight},
};


/// Get a shader of type (with params)
std::unique_ptr<IShader> getShader(ShaderType shaderType, float cutOff);


#endif // SHADERTYPE_HPP
